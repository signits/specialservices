---
# This is an example playbook to execute Ansible tests.

- name: Verify
  hosts: all
  become: yes

  tasks:
  - name: get service facts
    ansible.builtin.service_facts:

  - name: get package facts
    ansible.builtin.package_facts:
      manager: "auto"

  - name: ensure packages are not installed
    ansible.builtin.assert:
      that: ansible_facts.packages["{{ item }}"] is not defined
      fail_msg: "{{ item }} package is installed!"
      success_msg: "{{ item }} package is not installed"
    loop:
      - xorg-x11-server-Xorg
      - xorg-x11-server-common
      - xorg-x11-server-utils
      - xorg-x11-server-Xwayland
      - dhcp
      - openldap-servers
      - openldap-clients
      - bind
      - vsftpd
      - httpd
      - dovecot
      - samba
      - squid

  - name: ensure services are not installed
    ansible.builtin.assert:
      that: ansible_facts.services["{{ item }}"] is not defined
      fail_msg: "{{ item }} service is not disabled!"
      success_msg: "{{ item }} service is disabled"
    loop:
      - avahi-daemon
      - cups
      - nfslock
      - rpcgssd
      - rpcbind
      - rpcidmapd
      - rpcsvcgssd
      - rsyncd

  - name: check if /etc/systemd/system/default.target is configured
    ansible.builtin.stat: 
      path: /etc/systemd/system/default.target
    register: default_target

  - name: check if /etc/systemd/system/default.target is a link
    ansible.builtin.assert:
      that: default_target.stat.islnk is defined and default_target.stat.islnk
      fail_msg: "/etc/systemd/system/default.target is not a symlink"
      success_msg: "/etc/systemd/system/default.target is a symlink"

  - name: check if /etc/systemd/system/default.target points to /usr/lib/systemd/system/multi-user.target
    ansible.builtin.assert:
      that: default_target.stat.islnk is defined and default_target.stat.lnk_target == "/usr/lib/systemd/system/multi-user.target"
      fail_msg: "/etc/systemd/system/default.target points to wrong location {{ default_target.stat.lnk_target }}"
      success_msg: "/etc/systemd/system/default.target points to correct location {{ default_target.stat.lnk_target }}"

  - name: Stat file /etc/sysconfig/init
    ansible.builtin.stat:
      path: "/etc/sysconfig/init"
    register: initfile

  - name: Check mode of /etc/sysconfig/init
    ansible.builtin.assert:
      that: initfile.stat.mode == "0027"
      fail_msg: "/etc/sysconfig/init file has wrong mode: {{ initfile.stat.mode }}"
      success_msg: "/etc/sysconfig/init file has correct mode: {{ initfile.stat.mode }}"

  - name: check configuration /etc/postfix/main.cf
    ansible.builtin.lineinfile:
      path: "/etc/postfix/main.cf"
      line: "{{ item }}"
      state: present
    check_mode: yes
    register: postfix_register
    loop:
      - "inet_interfaces = localhost"
      - "inet_protocols = ipv4"

  - name: check if /etc/postfix/main.cf file is configured
    ansible.builtin.assert:
      that: item.changed == false and item.failed == false
      fail_msg: "{{ item.item }} is not configured in /etc/postfix/main.cf"
      success_msg: "{{ item.item }} is configured in /etc/postfix/main.cf"
    loop: "{{ postfix_register.results }}"