sepcialservices
=========

This module configures and/or disables and removes several services

Requirements
------------

None

Role Variables
--------------

enable_specialservices: true - [controls if the module will run at all]

specialservices_remove_ldap: true - [whether to remove ldap client from the system]

specialservices_disable_nfs: true - [whether to disable and stop nfs related services]

specialservices_remove_dns_server: true - [whether to removes DNS server (bind)]

specialservices_remove_httpd_server: true - [whether to remove HTTPD server (httpd)]

specialservices_remove_samba: true - [whteher to removes samba server]

specialservices_remove_httpd_proxy: true - [whether to remove HTTPD proxy server (squid)]

specialservices_remove_x_window_system: true - [whether to remove X11 server (xorg-x11-server-Xorg and xorg-x11-server-common) and disable X11]

specialservices_disable_rsync_service: true - [whteher to disable rsyncd service]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
